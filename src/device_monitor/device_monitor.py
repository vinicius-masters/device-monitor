from os import environ
from collections import defaultdict
from pyjung import JungRegistry


class DeviceMonitor(JungRegistry):

    def __init__(self, kafka_broker, in_topic, out_topic, group, port='9092'):

        super().__init__(kafka_broker, in_topic, out_topic, port, group)
        # monitor's storage should hold lists of readings organised by device ids
        self.storage = defaultdict(lambda: [])

    def process_message(self, message):
        task_id = message["id"]
        task = message["task"]
        device_id = message["content"]["device_id"]
        
        page_limit = None
        if "page_limit" in message["content"]:
            page_limit = message["content"]["page_limit"]

        response = {"task_id": task_id, "task": task}
        result = None

        if task == "READ":
            result = self.storage[device_id][:page_limit]
        elif task == "WRITE":
            reading = {"value": message["content"]["reading"],
                       "timestamp": message["content"]["timestamp"]}
            result = {"newRecord": reading, "device_id": device_id}
            self.storage[device_id].insert(0, reading)

        response["result"] = result
        return response


def main():
    kafka_broker = environ["KAFKA_BROKER"]
    device_monitor = DeviceMonitor(kafka_broker=kafka_broker,
                                   in_topic="reading_tasks",
                                   out_topic="reading_results",
                                   port="9092",
                                   group="device-monitor")
    device_monitor.process_tasks()


if __name__ == "__main__":
    main()
